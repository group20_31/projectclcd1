 <?php 
ob_start();
session_start();
$sess_user = $_SESSION['sess_user'];

$page = $_GET['page'];
?>
<!DOCTYPE html>
<html lang="en">
<?php include('includes/conn.php');?>
<?php include('includes/secret-key.php');?>
<?php include('includes/head.php');?>
  <body>
    <?php include ('includes/header.php');?>
    <?php
    if ($page == '' || $page == 'oonew') {
      include 'home.php';
    }
    if ($page == 'history'){
      include 'history.php';
    }
    if ($page == 'travel'){
      include 'travel.php';
    }
    if ($page == 'restaurant'){
      include 'restaurant.php';
    }
    if ($page == 'contact'){
      include 'contact.php';
    }
    ?>
    <?php include ('includes/footer.php');?>
</body>

</html>

<?php include('includes/script.php');?>
